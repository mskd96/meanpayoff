# MeanPayoff
An implementation of Strategy Improvement Algorithm for Mean Payoff Games

Link to the paper:

http://link.springer.com/chapter/10.1007/978-3-540-87531-4_27#page-1

Usage:

1. Go to src directory

2. make

3. "./mpg ../Examples/a.txt | grep Even" Run the executable with an example from "Examples" directory

4. You can also give examples manually by following these instructions 

Notes: 

- First word of first line should either be meanpayoff or parity, followed by a number ehich is (number of nodes you would like to place-1).
  Ex: If you want to place 10 node for a meanpayoff game you write the firste line as -- meanpayoff 9;
- You give the node information from the next line, one node per line.
- Format : player id, prefernce, player, successor list, name
- player id is a number starting from 0. preference essentially reduces to parity in case of parity game input and node weight in case of meanpayoff game input.
- Player is which player the node belongs to.
- successor list is list of id's separated by commas and name is just a notation for any given node (should be given in quotes).
