#!/bin/bash

time=Results/time.txt
ExampleFolder=./Examples/MyExamples

rm $time
echo -e "Input \t\t\t\t Output \t Mean Payoff" > $time


for (( n=70; n <= 100; n++ ))
do
	for (( c=10; c <= 10; c++ ))
	do
		for (( a=10; a <= 20; a++ ))
		do
			b=$((a+1))
			example="randomgame-$n-$c-$a-$b.pg"
			./pgsolver/bin/randomgame $n $c $a $b > $ExampleFolder/$example

			#zie="$(./symSIM1 -a 'zie' -i ./MyExamples/${example} | grep Even > Results/mpg.txt)"
			mpg="$(./src/mpg $ExampleFolder/${example} | grep { > Results/mpg.txt)"

			#zie="$(./symSIM1 -a 'zie' -i ./MyExamples/${example} | grep Time)"
			mpg="$(./src/mpg $ExampleFolder/${example} | grep Time)"

			#zie="$(echo $zie|awk -F" " '{print $2}')"
			#mpg="$(echo $mpg|awk -F" " '{print $2}')"
			zie="$(cat $ExampleFolder/${example} | pgsolver/bin/pgsolver -global recursive | 
				grep { | head -n 1 | tr -d ' ' > Results/zie.txt)"

			S1="$(diff Results/mpg.txt Results/zie.txt)"
			S2=""

			working="Correct"

			if [ "$S1" = "$S2" ]; then
				echo "Correct $example"
			else 
				working="Wrong"
				echo $S1
				cp $ExampleFolder/$example ImportantCases
				echo "Wrong $example"
			fi

			output="$example \t $working \t $mpg"
			echo -e $output >> $time
		done
	done
done
