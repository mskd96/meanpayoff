#include "Graph.hpp" 

#include <bits/stdc++.h>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <iterator>
#include <set>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <limits>
#include <ctime>
#include <stack>

#include <limits.h>


using namespace std;
using namespace MeanPayoffGame;

Graph::Graph() {
	n_vertices = 0;
}

void Graph::add_vertex(Vertex v) 
{
  this->vertices.push_back(v);
  //this->IdToVertex.insert(pair<int, Vertex*>(v.vid, &v));
  if (v.player == even) {
    	this->EvenVertices.push_back(n_vertices);
  }
  else {
    this->OddVertices.push_back(n_vertices);
  }
  
  n_vertices++; 
}

void Graph::add_vertex(long long int id, long long int priority, PLAYER player)
{
  //cout << "In add_vertex" << endl;	
  Vertex v(id, priority, player);
  //cout << "abcd " << v.get_id() << v.get_priority() << v.get_label() << v.get_player() << endl;
  /*
  cout << "1234" << endl;
  for(vector<Vertex>::iterator it = vertices.begin(); it!=vertices.end(); it++)
  {
	  cout << (*it).get_id() << " ";
  }
  cout << endl;
  */

  this->vertices.push_back(v);
  //this->IdToVertex.insert(pair<long long int, Vertex*>(v.vid, &v));
  if (v.player == even) {
    this->EvenVertices.push_back(n_vertices);
  }
  else {
    this->OddVertices.push_back(n_vertices);
  }
  
  n_vertices++; 
  //cout << "Out add_vertex" << endl;
}

void Graph::set_successor(long long int id, long long int succ)
{
  get_vertex(id).add_succ(succ);
  //cout << get_vertex(id).succ_list.size() << endl;
}

void Graph::fill_predecessors()
{
  vector<Vertex>::iterator i;

  // fill predecessors list
  for (i = vertices.begin(); i != vertices.end(); i++) {
    long long int vertex_id = (*i).vid;
    
    vector<long long int> succ_list = (*i).succ_list;
    vector<long long int>::iterator j = succ_list.begin();
    
    while (j != succ_list.end()) {
      get_vertex(*j).add_pred(vertex_id);
      j++;
    }
  }
}


Vertex& Graph::get_vertex(const long long int id)
{
  
  vector<Vertex>::iterator i;
  
  for (i = vertices.begin(); i != vertices.end(); i++) {
    if ((*i).vid == id) return (*i);
  }
  //if(IdToVertex.count(id)>0) return *IdToVertex[id];
  cout << "Error" << endl;
  throw VertexNotFound; 
}


Estimation Graph::UpdateEstimation(Estimation currentEstimate) {
	
	// Calculating Improvement Arena
	Graph ImprovementArena;
	//map<long long int, Vertex> IdToVertex;
	for(long long int i=0; i<(long long int)vertices.size(); i++) {
		Vertex curr = vertices[i];
		Vertex temp(curr.vid, curr.weight, curr.player);
		ImprovementArena.add_vertex(temp);
		//IdToVertex.insert(pair<long long int,Vertex>(curr.vid, ImprovementArena.get_vertex(curr.vid)));		
	}
	
	for(long long int i=0; i<(long long int)vertices.size(); i++) {
		Vertex curr = vertices[i];
		long long int CurrId = curr.vid;

		long long int count = 0;
		for(long long int j=0; j<(long long int)curr.succ_list.size(); j++) {
			long long int SuccId = curr.succ_list[j];
			Vertex Succ = get_vertex(SuccId);
			if(currentEstimate.estimate[SuccId].bestSum + Succ.weight 
				>= currentEstimate.estimate[CurrId].bestSum || 
				currentEstimate.estimate[SuccId].bestSum==PINF) {
				count++;
				ImprovementArena.set_successor(CurrId, SuccId);
			}
		}
		//if(curr.player==odd) assert(count==(long long int)curr.succ_list.size());
	}
	ImprovementArena.fill_predecessors();

	//cout << "Improvement Arena" << endl;
	//ImprovementArena.show();

	//Update Calculation from Improvement Arena
	map<long long int, Value> update;
	map<long long int, bool> isEvaluated;
	Value tempValue;
	Value zero(0);
	update[0] = zero;
	isEvaluated[0] = true;

	long long int level=0; // Player
	vector<long long int> candidates = ImprovementArena.get_vertex(0).pred_list;

	while(true) {// Calculating Update
		long long int size = candidates.size();
		vector<long long int> EvaluatedCandidates;
		map<long long int, bool> NextCandidates;

		bool ShouldExit = true;
		cout << "level : " << level << endl;
		for(long long int i=0; i<vertices.size(); i++) {
			cout << vertices[i].vid << " " << isEvaluated[vertices[i].vid] << 
			" " << update[vertices[i].vid].bestSum << endl;
			if(!isEvaluated[vertices[i].vid]) ShouldExit = false;
		}
		if(ShouldExit) break;
		cout << "Candidates" <<  endl;
		for(long long int i=0; i<size; i++) {
			cout << candidates[i] << " " ;
		}
		cout << endl;
		
		if(size==0) {
			level= 1;
			for(long long int i=0; i<vertices.size(); i++) {
				long long int CurrId = vertices[i].vid;
				if(!isEvaluated[CurrId]&&vertices[i].player==odd)
					candidates.push_back(CurrId);
			}
		}
		size = candidates.size();

		if(level == 1) {//all player 1 vertices
			//long long int CountNotEvaluated = 0;// Count of vertices not yet evaluated
			
			for(long long int i=0; i<size; i++) {
				bool AllEvaluated = true; // Are all Successors Evaluated
				Vertex curr = ImprovementArena.get_vertex(candidates[i]);
				long long int CurrId = curr.vid;
				//cout << "CurrId : " << CurrId << endl;
				//curr.show();
				assert(curr.player == odd);
				assert(CurrId = candidates[i]);
				long long int CaseNo = 1;
				long long int min = -1;
				for(long long int j=0; j<(long long int)curr.succ_list.size(); j++) {
					long long int SuccId = curr.succ_list[j];
					//cout << "SuccId : " << SuccId << endl;
					if(!isEvaluated[SuccId]) {
						AllEvaluated = false;
						//break;
					}
					else {
						long long int impr = currentEstimate.estimate[SuccId].bestSum + 
						get_vertex(SuccId).weight - currentEstimate.estimate[CurrId].bestSum;
						if(currentEstimate.estimate[SuccId].bestSum == PINF) {
							if(currentEstimate.estimate[CurrId].bestSum == PINF) {
								impr = 0;
							}
							else {
								impr = PINF;
							}
						}
						else if(currentEstimate.estimate[CurrId].bestSum == PINF) {
							assert(false);
						}
						long long int newVal;
						if(update[SuccId].bestSum==PINF || impr==PINF) {
							newVal = PINF;
						}
						else newVal = update[SuccId].bestSum + impr;
						if(update[SuccId].bestSum == 0 && impr == 0) CaseNo=2;
						if(min == -1) {min=newVal;}
						else if(newVal<min) {min=newVal;} 
					} 
				}
				//cout << "AllEvaluated " << AllEvaluated << endl;
				if(AllEvaluated) {
					tempValue.bestSum = min;
					update[CurrId] = tempValue;
					isEvaluated[CurrId] = true;
					EvaluatedCandidates.push_back(CurrId);
					cout << "Updated1 - " << CurrId << " " << min << endl;
				}
				else if(CaseNo==2) {// Case 2
					update[CurrId] = zero;
					isEvaluated[CurrId] = true;
					EvaluatedCandidates.push_back(CurrId);
					cout << "Updated2 - " << CurrId << " " << 0 << endl;
				}
				else {
					if(min==-1) {tempValue.bestSum = PINF;}
					else {tempValue.bestSum = min;}
					update[CurrId] = tempValue;
					isEvaluated[CurrId] = true;
					EvaluatedCandidates.push_back(CurrId);
					cout << "Updated4 - " << CurrId << " " << tempValue.bestSum << endl; 
					//CountNotEvaluated++;
				}
			}
			/*if(CountNotEvaluated == size) {//case 4
				tempValue.bestSum = PINF;
				for(long long int i=0; i<size; i++) {
					long long int CurrId = candidates[i];
					update[CurrId] = tempValue;
					isEvaluated[CurrId] = true;
					EvaluatedCandidates.push_back(CurrId);
					cout << "Updated4 - " << CurrId << " " << PINF << endl; 
				}
			}*/
			level = 0;
			
			//break;
		}
		else {//Player 0 vertices
			long long int CountNotEvaluated = 0;// Count of vertices not yet evaluated
			for(long long int i=0; i<size; i++) {
				bool AllEvaluated = true; // Are all Successors Evaluated
				Vertex curr = ImprovementArena.get_vertex(candidates[i]);
				//curr.show();
				long long int CurrId = curr.vid;
				assert(curr.player == even);
				assert(CurrId = candidates[i]);
				long long int max = -1;
				for(long long int j=0; j<(long long int)curr.succ_list.size(); j++) {
					long long int SuccId = curr.succ_list[j];
					//cout << "SuccId : " << SuccId << endl;
					if(!isEvaluated[SuccId]) {
						AllEvaluated = false;
						break;
					}
					else {
						long long int impr = currentEstimate.estimate[SuccId].bestSum + 
						get_vertex(SuccId).weight - currentEstimate.estimate[CurrId].bestSum;
						if(currentEstimate.estimate[SuccId].bestSum == PINF) {
							if(currentEstimate.estimate[CurrId].bestSum == PINF) {
								impr = 0;
							}
							else {
								impr = PINF;
							}
						}
						else if(currentEstimate.estimate[CurrId].bestSum == PINF) {
							assert(false);
						}

						long long int newVal;
						if(update[SuccId].bestSum==PINF || impr==PINF) {
							newVal = PINF;
						}
						else newVal = update[SuccId].bestSum + impr;
						//cout << impr << " " << newVal << endl;
						if(max == -1) {max = newVal;}
						else {if(newVal>max) {max = newVal;}}
					}
				}
				if(AllEvaluated) {
					tempValue.bestSum = max;
					update[CurrId] = tempValue;
					isEvaluated[CurrId] = true;
					EvaluatedCandidates.push_back(CurrId);
					cout << "Updated3 - " << CurrId << " " << tempValue.bestSum << endl;
				}
				else {
					CountNotEvaluated++;
				}
			}
			if(CountNotEvaluated == size) {//case 4
				tempValue.bestSum = PINF;
				for(long long int i=0; i<size; i++) {
					long long int CurrId = candidates[i];
					update[CurrId] = tempValue;
					isEvaluated[CurrId] = true;
					EvaluatedCandidates.push_back(CurrId);
					cout << "Updated5 - " << CurrId << " " << PINF << endl; 
				}
			}
			level = 1;
			//break;
		}

		// Find all the predecessors of Evaluated Candidates
		for(long long int i=0; i<(long long int)EvaluatedCandidates.size(); i++) {
			long long int CurrId = EvaluatedCandidates[i];
			vector<long long int> TempList = ImprovementArena.get_vertex(CurrId).pred_list;
			for(long long int j=0; j<(long long int)TempList.size(); j++) {
				long long int PredId = TempList[j];
				NextCandidates[PredId] = true;
			}
		}
		candidates.clear();
		for(long long int i=0; i<(long long int)vertices.size(); i++) {
			long long int CurrId = vertices[i].vid;
			if(NextCandidates[CurrId] && !isEvaluated[CurrId]) 
				candidates.push_back(CurrId);
		}
	}
	
	Estimation ret;
	for(long long int i=0; i<(long long int)vertices.size(); i++) {
		long long int CurrId = vertices[i].vid;
		if(isEvaluated[CurrId]) {
			ret.estimate[CurrId] = update[CurrId] + currentEstimate.estimate[CurrId];
			//assert(update[CurrId].bestSum>0);
		}
		else {
			assert(false);
			ret.estimate[CurrId] = Value(PINF); 
			cout << "What to do?" << endl;
		}
		//assert(ret.estimate[CurrId].bestSum>=0);
	}
	return ret;
	
}


vector<long long int> Graph::OptimalStrategyImprovement() {
	Estimation InitialEstimate, Prev, Next;
	Value zero(0);

	// Creating the first estimate
	for(long long int i=0; i<(long long int)vertices.size(); i++) {
		Vertex Curr = vertices[i];
		long long int CurrId = Curr.vid;
		if(Curr.player==even) {
			InitialEstimate.AddEstimate(CurrId, zero);
		}
		else {
			if(CurrId == 0) {
				InitialEstimate.AddEstimate(0,zero);
				continue;
			}
			long long int min = -1;
			for(long long int j=0; j<(long long int)Curr.succ_list.size(); j++) {
				long long int SuccId = Curr.succ_list[j];
				long long int temp = get_vertex(SuccId).weight;
				if(min == -1) {
					min = temp;
				}
				else {
					if(temp<min) min = temp;
				}
			}
			Value TempVal(min);
			InitialEstimate.AddEstimate(CurrId, TempVal);
		}
	}
	

	Prev = InitialEstimate;
	while(true) {
		show(Prev);
		Next = UpdateEstimation(Prev);
		if(Next==Prev) break;// When the current estimation is equal to next, we stop
		Prev = Next;
	}
	vector<long long int> ret;
	for(long long int i=0; i<(long long int)vertices.size(); i++) {
		if(Next.estimate[i].bestSum == PINF) ret.push_back(i);
	}
	return ret;
}


void Graph::show() {
	cout << "Graph" << endl;
	for(long long int i=0; i<(long long int)vertices.size(); i++) {
		vertices[i].show();
		Vertex Curr = get_vertex(vertices[i].vid);
		cout << "Successors : ";
		for(long long int j=0; j<(long long int)Curr.succ_list.size(); j++) {
			cout << Curr.succ_list[j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void Graph::show(Estimation e) {
	cout << "id Est" << endl;
	for(long long int i=0; i<(long long int)vertices.size(); i++) {
		cout << vertices[i].vid << " " << e.estimate[vertices[i].vid].bestSum << endl;
	}
	cout << endl;
}


void Graph::output_dot(ostream& out)
{
  vector<Vertex>::iterator i;
  
  out << "digraph G { \n";
  for (i = vertices.begin(); i != vertices.end(); i++) {
    long long int temp = (*i).vid;
    //temp+=1;
    ostringstream ss;
    ss << temp;
    string v = ss.str();

    if ((*i).player == odd) {
      out << v <<"[label=\"\\N (" << (*i).weight << ")\", color=black, shape=polygon ]\n";
    }
    else {
      out << v <<"[label=\"\\N (" << (*i).weight << ")\" , color=black ]\n";
    }
    
    vector<long long int> succ_list = (*i).succ_list;
    vector<long long int>::iterator j = succ_list.begin();
    
    while (j != succ_list.end()) {
      out << v << "->"<< get_vertex(*j).vid <<"\n";
      j++;
    }
  }
  out << "}\n";
}


void Graph::showG()
{
  string dot_file = "all.dot";
  string pdf_file = "all.pdf";
  ofstream outs(dot_file);
    
  this->output_dot(outs);
  outs.close();

  string cmd1 = "dot -Tpdf " + dot_file + " -o" + pdf_file;
  string cmd2 = "xdg-open " + pdf_file + "&"; 
   
  system(cmd1.c_str());
  system(cmd2.c_str());
  
  getchar();
  
}
 